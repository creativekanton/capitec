# Capitec Assessment.

1. Summary of design choice:
	a. I chose to design the app using a little bit of Capitec's CI to cater for the design work, The reason why I chose this route was because I was limited from using frameworks to build
	the widget.
	
2. Challenges I came across.
	a. Finding API to display accurate data
	b. Collecting the right information from the API
	c. Working  with Vanilla web components 
	d. Naming conversion from the API were a hassle, which made it difficult to pull data 
	
3. Lessons.
  a. Custom web components made with Vanilla.js are really awesome and would like to learn more about them
  b. Time management and project management skills. 
  c. Communication skills 
  
	
	