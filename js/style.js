// Buy shares at Capitec
// Author: Glen Mudau
// Topic: Capitec Assessment 2019
// Version: 1.0
// Date: March 2019

var Http = new XMLHttpRequest();
var url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&interval&symbol=CPI.JOH&from_currency=ZAR&datatype=json&apikey=ON9H6T316JX7KTGNf&results=1';
Http.open("GET", url);
Http.send();

var answer = null;
var clientMoney = null;

Http.onreadystatechange = (e) => {
	if (answer) return;
	var anything = JSON.parse(Http.responseText);
	var newVariable = Object.values(anything['Time Series (Daily)']);
	answer = newVariable[0]["4. close"];
}
function myFunction() {
  return (clientMoney / answer).toFixed(5)
}

function loadDoc() {
 	clientMoney= document.getElementById("textInput").value;
  document.getElementById("shares").innerHTML = myFunction();

}
